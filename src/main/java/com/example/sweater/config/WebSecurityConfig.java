package com.example.sweater.config;


import com.example.sweater.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity(debug = true)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserService userService;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public WebSecurityConfig(UserService userService){
        this.userService = userService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .authorizeRequests()
                    .antMatchers("/","/registration","/static/**", "/activate/*").permitAll() // тут я через юрл / і /registration пускає всіх
                  //Дозволений доступ по силках / //registration доступ всім користувачам навіть не залгіненим permitAll()
                    .anyRequest().authenticated()  // якшо йдуть певні переходи по силка то юзер має бути аутентифікований
                  // anyRequest() - на всі подільші силки мають доступ лиш аутентифіковані користувачі .authenticated()
                .and()
                    .formLogin()
                    // викликликаємо форму логування на строінці вказаній нижче в .loginPage("/login")
                    .loginPage("/login") // перекидає мене по дефолту на логування
                    .permitAll()
                    // Дозволяєсо аунтентифікуватися всіх користувачаи незалежно чи він аутентифікований чи ні
                .and()
                    .rememberMe()
                .and()
                    .logout()
                    .permitAll(); // означає якшо по логауту перейдеш з логіну то воно тебе пропустить
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }
}
