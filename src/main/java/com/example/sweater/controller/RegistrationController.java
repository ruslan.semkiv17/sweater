package com.example.sweater.controller;

import com.example.sweater.domain.User;
import com.example.sweater.domain.dto.CaptchaResponseDto;
import com.example.sweater.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Controller
public class RegistrationController {
    private static final String CAPTURE_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s";

    private final UserService userService;
    private final RestTemplate restTemplate;
    @Value("${recaptcha.secret}")
    private String recaptchaSecret;

    @Autowired
    public RegistrationController(UserService userService, RestTemplate restTemplate){
        this.userService = userService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(
            @RequestParam String passwordConfirm,
            @RequestParam("g-recaptcha-response") String captchaResponse,
            @Valid User user,
            BindingResult bindingResult ,
            Model model){
        System.out.println(user);

        String url = String.format(CAPTURE_URL, recaptchaSecret, captchaResponse);
        CaptchaResponseDto capResponse = restTemplate.postForObject(url, Collections.emptyList(), CaptchaResponseDto.class);

        if(!capResponse.isSuccess()){
            model.addAttribute("captchaError","Fill captcha");
        }

        boolean emptyConfPass = StringUtils.isEmpty(passwordConfirm);
        if(emptyConfPass){
            model.addAttribute("passwordConfirmError","Confirm password cannot be empty");
        }

        if(user.getPassword()!=null && !user.getPassword().equals(passwordConfirm)){
            model.addAttribute("passwordError","Password are different!");
        }

        if(emptyConfPass || bindingResult.hasErrors() || !capResponse.isSuccess()){
            Map<String, String> errorMessage = ControllerUtils.getErrorMessage(bindingResult);

            model.mergeAttributes(errorMessage);

            return "registration";
        }

        if(!userService.addUser(user)){
            model.addAttribute("usernameError","User exists!");
            return "registration";}

        return "redirect:/login";
    }

    @GetMapping("/activate/{code}")
    private String activate(@PathVariable String code, Model model){
        boolean isActivated = userService.activateUser(code);

        if(isActivated){
            model.addAttribute("messageType","success");
            model.addAttribute("message","User has been successfully activated");
        }else {
            model.addAttribute("messageType","danger");
            model.addAttribute("message","Email wasn't activated");
        }

        return "login";
    }
}
