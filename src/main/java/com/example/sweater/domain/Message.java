package com.example.sweater.domain;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank(message = "Please write some text")
    @NotNull(message = "Please write some text")
    @Length(max = 2048,message = "Message too long")
    private String text;

    @Length(max = 255,message = "Tag too long")
    private String tag;

    @ManyToOne(fetch = FetchType.EAGER) //
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User author;

    private String filename;

    public String getAuthorName(){
        return author!=null ? author.getUsername() : "<none>";
    }
}
