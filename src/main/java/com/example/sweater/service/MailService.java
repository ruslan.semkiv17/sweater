package com.example.sweater.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailService {
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String username;

    @Autowired
    public MailService(@Qualifier("getMailSender") JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    public void send(String emailTo,String subject,String message){
        SimpleMailMessage simpleMail = new SimpleMailMessage();

        simpleMail.setFrom(username);
        simpleMail.setTo(emailTo);
        simpleMail.setSubject(subject);
        simpleMail.setText(message);

        javaMailSender.send(simpleMail);
    }
}
