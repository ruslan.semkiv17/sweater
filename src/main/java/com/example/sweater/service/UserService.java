package com.example.sweater.service;

import com.example.sweater.domain.Role;
import com.example.sweater.domain.User;
import com.example.sweater.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

// Анотація сервіс для бізнес логіки для перевірки обєкта на різні поля ,наприклад перед створенням в методі якомусь
// ддя цього і є сервіс
@Service
public class UserService implements UserDetailsService {
    private final UserRepo userRepo;
    private final MailService mailSender;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserService(UserRepo userRepo, MailService mailSender){
        this.userRepo = userRepo;
        this.mailSender = mailSender;
//        this.passwordEncoder = passwordEncoder;
    }

    @Bean("bCrypt")
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder(12);
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User byUsername = userRepo.findByUsername(username);
            if(byUsername == null){
                throw new UsernameNotFoundException("User wasn't found");
            }
        return byUsername;  // Підтяє його імя з паролями і перевіряє чи такий вєе є якщо є то не пускає
    }

    public boolean addUser(User user){
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if(userFromDb!=null){
            return false;
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(Role.USER));
        user.setActivationCode(UUID.randomUUID().toString());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepo.save(user);

        sendMessage(user);
        return true;
    }

    private void sendMessage(User user) {
        if(!StringUtils.isEmpty(user.getEmail())){
            String message = String.format("Hello %s \n Welcome to Sweater. Please , click on link,to verify your email !" +
                    "link: http://localhost:7777/activate/%s", user.getUsername(), user.getActivationCode());

            mailSender.send(user.getEmail(),"Activation code",message);
        }
    }

    public boolean activateUser(String code) {
        User user  = userRepo.findByActivationCode(code);
        if(user == null){
            return false;
        }
        user.setActivationCode(null);
        userRepo.save(user);

        return true;
    }

    public List<User> findAll() {
        return userRepo.findAll();
    }

    public void delete(User user) {
        userRepo.delete(user);
    }

    public void saveUser(User user, String userName, Map<String, String> form) {

        user.setUsername(userName);

        Set<String> userRoles = Arrays.stream(Role.values())
                .map(Role::name)
                .collect(Collectors.toSet());

        user.getRoles().clear();

        for (String key: form.keySet()){
            if(userRoles.contains(key)){
                user.getRoles().add(Role.valueOf(key));
            }
        }
        userRepo.save(user);
    }

    public void updateProfile(User user, String password, String email) {
        String userEmail = user.getEmail();

        boolean isEmailChanged  = (email != null && !email.equals(userEmail)) ||
                (userEmail!=null && !userEmail.equals(email));
        if(isEmailChanged){
            user.setEmail(email);

            if(!StringUtils.isEmpty(email)){
                user.setActivationCode(UUID.randomUUID().toString());
            }
        }
        if(!StringUtils.isEmpty(password)){
            user.setPassword(password);
        }
        userRepo.save(user);

        if (isEmailChanged){
        sendMessage(user);}

    }
}
